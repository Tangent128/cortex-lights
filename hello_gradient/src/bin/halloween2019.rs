#![no_std]
#![no_main]

use core::iter::repeat;
use lights::{
    gamma::correct,
    HardwareRgb,
    PixelIterator,
    rgb::{
        Rgb
    }
};
use lights_hal::{boot, delay, entry};

#[entry]
fn main() -> ! {
    let mut lights = boot();
    lights.heartbeat();

    let pattern =
        repeat(&Rgb(255,0,255)).take(150)
        .chain(
            repeat(&Rgb(255,128,0)).take(150)
        ).cycle();

    let mut buffer = [HardwareRgb(255,255,0); 200];
    loop {
        // blink light to demonstrate loop is still running
        lights.heartbeat();

        for (i, pixel) in pattern.clone().take(200).enumerate() {
            buffer[i] = correct(pixel);
        }

        buffer.iter().render_to(&mut lights);

        delay(12_000_000);
    }
}
