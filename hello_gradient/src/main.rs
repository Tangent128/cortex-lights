#![no_std]
#![no_main]

use core::iter::repeat;
use lights::{
    gamma::GammaCorrector,
    PixelIterator,
    rgb::{
        gray,
        linear_gradient,
        Rgb
    }
};
use lights_hal::{boot, delay, entry};

#[entry]
fn main() -> ! {
    let mut lights = GammaCorrector(boot());

    let red = Rgb(255, 0, 0);
    let yellow = Rgb(255, 255, 0);
    let green = Rgb(0, 255, 0);
    let teal = Rgb(0, 128, 255);
    let purple = Rgb(128, 0, 255);

    let segment_length = 60;

    let mut gradient = linear_gradient(red, yellow, segment_length)
        .chain(linear_gradient(yellow, green, segment_length))
        .chain(linear_gradient(green, teal, segment_length))
        .chain(linear_gradient(teal, purple, segment_length))
        .chain(linear_gradient(purple, gray(0), segment_length))
        .chain(repeat(gray(255)).take(60)).cycle();

    loop {
        gradient.clone().take(60).render_to(&mut lights);

        gradient.next();
        delay(1_000_000);
    }
}
